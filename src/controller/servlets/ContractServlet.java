package controller.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.blocks.Blockchain;
import models.entity.ActionException;
import models.entity.BetContract;
import models.entity.Person;

@WebServlet("/ContractServlet")
public class ContractServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ContractServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Blockchain chain = Blockchain.getInstance();
		Person bettor = chain.getPeople().get(request.getParameter("bettor"));
		Person respondent = chain.getPeople().get(request.getParameter("respondent"));
		int bet = Integer.valueOf(request.getParameter("bet"));
		String url = request.getParameter("url");
		BetContract contract = new BetContract(bettor.getName(), respondent.getName(), bet, url);
		chain.addContract(contract);
//		request.getRequestDispatcher("/index").forward(request, response);
		response.sendRedirect("/BlockchainProject/index");
	}
}
