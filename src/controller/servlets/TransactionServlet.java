package controller.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.blocks.Blockchain;
import models.entity.ActionException;
import models.entity.Person;
import models.entity.Transaction;

@WebServlet("/TransactionServlet")
public class TransactionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Blockchain chain = Blockchain.getInstance();
		Person sender = chain.getPeople().get(request.getParameter("sender"));
		Person receiver = chain.getPeople().get(request.getParameter("receiver"));
		int amount = Integer.valueOf(request.getParameter("amount"));
		Transaction t;
		try {
			t = new Transaction(sender.getName(), receiver.getName(), amount);
			t.act();
			Blockchain.getInstance().addNewBlock(t);
			request.setAttribute("isChanged", true);
		} catch (ActionException e) {
			request.setAttribute("error_message", e.getCause());
		}
//		request.getRequestDispatcher("/index").forward(request, response);
		response.sendRedirect("/BlockchainProject/index");
	}

}
