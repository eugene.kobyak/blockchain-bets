package controller.servlets;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.blocks.Blockchain;

@WebServlet("/NodeServlet")
public class NodeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public NodeServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Blockchain chain = Blockchain.getInstance();
		Blockchain newChain;
		try {
			System.out.println("receiving");
			newChain = receiveBlockChain(request.getParameter("blockchain"));
			chain.replaceChain(newChain);
			response.setStatus(202);
		} catch (ClassNotFoundException e) {
			response.setStatus(415);
		}
		System.out.println("received");
	}

	private Blockchain receiveBlockChain(String chain) throws IOException, ClassNotFoundException {
		ByteArrayInputStream bais = new ByteArrayInputStream(chain.getBytes());
		ObjectInputStream ois = new ObjectInputStream(bais);
		Blockchain blockchain = (Blockchain) ois.readObject();
		ois.close();
		return blockchain;
	}
}
