package controller.servlets;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.blocks.Block;
import models.blocks.Blockchain;
import models.entity.Person;

@WebServlet("/AuthServlet")
public class AuthServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("/JSP/auth.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Blockchain chain = Blockchain.getInstance();
		Person person = chain.getPeople().get(request.getParameter("login"));
		String password = getHash(request.getParameter("password"));
		if (person != null) {
			if (person.getPasswordHash().equals(password)) {
				request.getSession().setAttribute("user", person);
				response.sendRedirect("/BlockchainProject/index");
//				request.getRequestDispatcher("/index").forward(request, response);
			} else {
				request.getSession().setAttribute("errorMessage", "Password is incorrect");
				request.getRequestDispatcher("JSP/auth.jsp").forward(request, response);
			}
		} else {
			request.getSession().setAttribute("errorMessage", "Login is incorrect");
			request.getRequestDispatcher("JSP/auth.jsp").forward(request, response);
		}
	}

	private String getHash(String pass) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			return Block.getHexStringFromBytes(md.digest(pass.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}

}
