package controller.websocket;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import models.blocks.Block;
import models.blocks.Blockchain;
import models.entity.BetContract;
import models.entity.Person;
import models.event.EventListener;

@ApplicationScoped
@ServerEndpoint(value = "/actions", configurator = GetHttpSessionConfigurator.class)
public class DeviceWebSocketServer {

	private HttpSession httpSession;
	private Session session;

	private EventListener<List<Block>> chainSender = (t) -> sendChain(session);
	private EventListener<HashMap<Integer, BetContract>> contractSender = (t) -> sendContracts(session);

	@OnOpen
	public void open(Session session, EndpointConfig config) {
		this.session = session;
		this.httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());
		Blockchain blockchain = Blockchain.getInstance();
		blockchain.addBlockChangedListener(chainSender);
		blockchain.addContractChangedListener(contractSender);
		sendChain(session);
		sendContracts(session);
	}

	private void sendChain(Session session) {
		sendMessageToClient(getChainString(), session);
	}

	private void sendContracts(Session session) {
		sendMessageToClient(getContractsString(), session);
	}

	private synchronized void sendMessageToClient(String message, Session session) {
		if (session.isOpen()) {
			try {
				session.getBasicRemote().sendText(message);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private boolean checkContract(BetContract contract) {
		String currentUser = ((Person) httpSession.getAttribute("user")).getName();
		if (!currentUser.equals(contract.getBettor()) && !currentUser.equals(contract.getResponder())) {
			return false;
		}
		if ((contract.isAccptedByBettor() != null) && (contract.isAccptedByResponder() != null)) {
			if (contract.isAccptedByBettor() && contract.isAccptedByResponder()) {
				return false;
			}
			if (!contract.isAccptedByBettor() || !contract.isAccptedByResponder()) {
				return false;
			}
		}
		if (contract.isAccptedByBettor() == null) {
			if (contract.isAccptedByResponder() != null && contract.isAccptedByResponder() == false) {
				return false;
			}
		}
		if (contract.isAccptedByResponder() == null) {
			if (contract.isAccptedByBettor() != null && contract.isAccptedByBettor() == false) {
				return false;
			}
		}
		return true;

	}

	private String getContractsString() {
		JsonObject jlist = new JsonObject();
		JsonArray jcontracts = new JsonArray();
		for (BetContract contract : Blockchain.getInstance().getContracts()) {
			if (checkContract(contract)) {
				JsonObject jcontract = new JsonObject();
				jcontract.addProperty("bettor", contract.getBettor());
				jcontract.addProperty("responder", contract.getResponder());
				jcontract.addProperty("bet", contract.getBet());
				jcontract.addProperty("url", contract.getCondition().getUrl());
				jcontract.addProperty("bettorSign", contract.isAccptedByBettor());
				jcontract.addProperty("responderSign", contract.isAccptedByResponder());
				jcontract.addProperty("index", contract.hashCode());
				jcontract.addProperty("currentUser", ((Person) httpSession.getAttribute("user")).getName());
				jcontracts.add(jcontract);
			}
		}
		jlist.add("contracts", jcontracts);
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		return gson.toJson(jlist);
	}

	private String getChainString() {
		JsonObject jchain = new JsonObject();
		JsonArray jblocks = new JsonArray();
		for (Block block : Blockchain.getInstance().getBlocks()) {
			JsonObject jblock = new JsonObject();
			jblock.addProperty("index", block.getIndex());
			jblock.addProperty("previousHash", block.getPreviousHash().substring(0, 6));
			jblock.addProperty("hash", block.getHash().substring(0, 6));
			jblock.addProperty("data", block.getData().toString());
			jblocks.add(jblock);
		}
		jchain.add("chain", jblocks);
		JsonObject jPerson = new JsonObject();
		Person currentUser = ((Person) httpSession.getAttribute("user"));
		jPerson.addProperty("currentUser", currentUser.getName());
		jPerson.addProperty("amount", Blockchain.getInstance().getPeople().get(currentUser.getName()).getBankAccount());
		jchain.add("person", jPerson);
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		return gson.toJson(jchain);
	}

	@OnClose
	public synchronized void close(Session session) {
		Blockchain.getInstance().removeBlockChangedListener(chainSender);
		Blockchain.getInstance().removeContractChangedListener(contractSender);
	}

	@OnError
	public synchronized void onError(Throwable error) {
		// error.printStackTrace();
		Blockchain.getInstance().removeBlockChangedListener(chainSender);
		Blockchain.getInstance().removeContractChangedListener(contractSender);
	}

	@OnMessage
	public synchronized void handleMessage(String message, Session session) {
		Person user = (Person) httpSession.getAttribute("user");
		JsonParser parser = new JsonParser();
		JsonElement jMessage = parser.parse(message);
		if (jMessage.isJsonObject()) {
			JsonObject jsonObject = jMessage.getAsJsonObject();
			int contractHash = jsonObject.get("index").getAsInt();
			String answer = jsonObject.get("answer").getAsString();
			BetContract currentContract = Blockchain.getInstance().getContract(contractHash);

			if (!(currentContract == null)) {
				Blockchain.getInstance().updateContract(contractHash, answer,
						user.getName().equals(currentContract.getBettor()),
						user.getName().equals(currentContract.getResponder()));
			}
		}
	}
}
