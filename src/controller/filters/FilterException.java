package controller.filters;

import javax.servlet.ServletException;

public class FilterException extends ServletException {

	private static final long serialVersionUID = -3858653964273707807L;

	public FilterException() {
		super();
	}

	public FilterException(String message) {
		super(message);
	}

	public FilterException(String message, Throwable cause) {
		super(message, cause);
	}

	public FilterException(Throwable cause) {
		super(cause);
	}
}
