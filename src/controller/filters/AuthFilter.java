package controller.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.entity.Person;

@WebFilter(urlPatterns = { "/*" }, asyncSupported = true)
public class AuthFilter implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) req;
		HttpServletResponse httpServletResponse = (HttpServletResponse) resp;

		Person person = (Person) httpServletRequest.getSession().getAttribute("user");
		if (person != null || isUrlAvailable(httpServletRequest.getRequestURI())) {
			chain.doFilter(req, resp);
		} else {
			httpServletResponse.sendRedirect("JSP/auth.jsp");
		}
	}

	private boolean isUrlAvailable(String url) {
		return url.endsWith(".js") || url.endsWith("AuthServlet") || url.endsWith(".jsp") || url.endsWith(".css")
				|| url.endsWith(".html") || url.endsWith("NodeServlet");
	}

	public void destroy() {
	}

	public void init(FilterConfig config) throws ServletException {
	}

}
