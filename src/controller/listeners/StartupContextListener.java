package controller.listeners;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Jsoup;

import models.blocks.Blockchain;

@WebListener()
public class StartupContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		Blockchain.getInstance().removeBlockChangedListener((t) -> sendBlockchain(Blockchain.getInstance()));
		Blockchain.getInstance().removeContractChangedListener((t) -> sendBlockchain(Blockchain.getInstance()));
		ServletContextListener.super.contextDestroyed(sce);
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Blockchain.getInstance().addBlockChangedListener((t) -> sendBlockchain(Blockchain.getInstance()));
		Blockchain.getInstance().addContractChangedListener((t) -> sendBlockchain(Blockchain.getInstance()));
		ServletContextListener.super.contextInitialized(sce);
	}

	private void sendBlockchain(Blockchain chain) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(chain);
			// Response response =
			// Jsoup.connect("http://172.20.15.159:8080/BlockchainProject/NodeServlet")
			// .data("blockchain", baos.toString()).method(Method.POST).execute();
//			Connection con = Jsoup.connect("http://172.20.15.159:8080/BlockchainProject/NodeServlet");
//			Connection.Response response = con.data("blockchain", baos.toString()).method(Method.POST).execute();
//			// if (response.statusCode() <300) {
			//
			// }
			System.out.println(sendString("blockchain", baos.toString()));
			oos.close();
			baos.close();
//			if (response.statusCode() >= 400) {
//				System.out.println("not ok " + response.statusCode());
//			} else
//				System.out.println("OK " + response.statusCode());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private int sendString(String name, String data) {
		Connection con = Jsoup.connect("http://172.20.15.159:8080/BlockchainProject/NodeServlet");
		Connection.Response response;
		try {
			response = con.data(name, data).method(Method.POST).execute();
			return response.statusCode();
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			return 404;
		}
	}

}
