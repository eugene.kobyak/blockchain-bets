package models.event;

public interface EventListener<T> {

	void onEvent(T t);
}
