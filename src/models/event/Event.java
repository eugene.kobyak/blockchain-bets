package models.event;

public interface Event<T> {
	
	
	void addEventListener(EventListener<T> listener);
	
	void removeEventListener(EventListener<T> listener);
	
	
	public void occur(T t);

}
