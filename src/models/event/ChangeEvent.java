package models.event;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChangeEvent<T> implements Event<T>, Serializable {

	private static final long serialVersionUID = -8432160588748958599L;
	private List<EventListener<T>> listeners;
	private ExecutorService service = Executors.newCachedThreadPool();

	@Override
	public void addEventListener(EventListener<T> listener) {
		listeners.add(listener);
	}

	@Override
	public void removeEventListener(EventListener<T> listener) {
		listeners.remove(listener);
	}

	public ChangeEvent() {
		this.listeners = new LinkedList<>();
	}

	@Override
	public synchronized void occur(T t) {
		for (EventListener<T> listener : listeners) {
			service.execute(() -> listener.onEvent(t));
//			listener.onEvent(t);
		}
	}

}
