package models.blocks;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import models.entity.ActionException;
import models.entity.BetContract;
import models.entity.Person;
import models.event.ChangeEvent;
import models.event.Event;
import models.event.EventListener;

public class Blockchain implements Serializable {

	private static final long serialVersionUID = 6159528824006664071L;
	private static Blockchain instance = new Blockchain();
	private ArrayList<Block> blocks;
	private Map<String, Person> people;
	private HashMap<Integer, BetContract> contracts;

	private transient Event<List<Block>> blocksChangeEvent;
	private transient Event<HashMap<Integer, BetContract>> contractsChangeEvent;

	private Blockchain() {
		this.people = new TreeMap<>();
		this.blocks = new ArrayList<Block>();
		this.blocks.add(Block.getGenesisBlock());
		this.people.put("valera",
				new Person(1000, "valera", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f"));
		this.people.put("eugene",
				new Person(1000, "eugene", "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f"));
		this.contracts = new HashMap<Integer, BetContract>();
		this.blocksChangeEvent = new ChangeEvent<>();
		this.contractsChangeEvent = new ChangeEvent<>();
	}

	public Map<String, Person> getPeople() {
		return people;
	}

	public static Blockchain getInstance() {
		// TODO: do real singleton
		return instance;
	}

	public Block getLastBlock() {
		return blocks.get(blocks.size() - 1);
	}

	public void addNewBlock(Object blockData) {
		Block previousBlock = instance.getLastBlock();
		Block newBlock = new Block(previousBlock, blockData);
		if (previousBlock.isValidNextBlock(newBlock)) {
			this.blocks.add(newBlock);
			blocksChangeEvent.occur(blocks);
			// System.out.println("chain is: " + isValidChain(getInstance().blocks));
		} else {
			// System.out.println("invalid new block");
		}
	}

	public static boolean isValidChain(ArrayList<Block> newChain) {
		Iterator<Block> i = newChain.iterator();
		Block a = i.next();
		while (i.hasNext()) {
			Block b = i.next();
			if (!a.isValidNextBlock(b)) {
				System.out.println(" invalid block b: " + b + ", a: " + a);
				return false;
			}
			a = b;
		}
		return true;
	}

	public void replaceChain(Blockchain newChain) {
		if (newChain.blocks.size() > this.blocks.size()) {
			if (isValidChain(newChain.blocks)) {
				this.blocks = newChain.blocks;
				this.people = newChain.people;
				blocksChangeEvent.occur(blocks);
			} else {
				System.out.println("Received blockchain invalid");
			}
		} else {
			System.out.println("The same(less) length");
		}
		if (mergeContracts(newChain.contracts)) {
			contractsChangeEvent.occur(contracts);
		}
	}

	private boolean mergeContracts(HashMap<Integer, BetContract> newContractList) {
		boolean isAnyContractChanged = false;
		for (BetContract newContract : newContractList.values()) {
			BetContract contract = contracts.get(newContract.hashCode());
			if (contract != null) {
				isAnyContractChanged = contract.updateSignatures(newContract);
			} else {
				contracts.put(newContract.hashCode(), newContract);
				isAnyContractChanged = true;
			}
		}
		return isAnyContractChanged;
	}

	public ArrayList<Block> getBlocks() {
		return blocks;
	}

	public synchronized void addBlockChangedListener(EventListener<List<Block>> listener) {
		blocksChangeEvent.addEventListener(listener);
	}

	public synchronized void removeBlockChangedListener(EventListener<List<Block>> listener) {
		blocksChangeEvent.removeEventListener(listener);
	}

	public synchronized void addContractChangedListener(EventListener<HashMap<Integer, BetContract>> listener) {
		contractsChangeEvent.addEventListener(listener);
	}

	public synchronized void removeContractChangedListener(EventListener<HashMap<Integer, BetContract>> listener) {
		contractsChangeEvent.removeEventListener(listener);
	}

	public synchronized void addContract(BetContract contract) {
		this.contracts.put(contract.hashCode(), contract);
		contractsChangeEvent.occur(contracts);
	}


	public BetContract getContract(Integer hash) {
		return contracts.get(hash);
	}

	public synchronized Collection<BetContract> getContracts() {
		return contracts.values();
	}

	public void updateContract(Integer hash, String isAccepted, boolean isBettor, boolean isResponder) {
		BetContract contract = contracts.get(hash);
		Boolean isUpdated = false;
		if (isAccepted.equals("accept")) {
			if (isBettor) {
				contract.setAccptedByBettor(true);
				isUpdated = true;
			}
			if (isResponder) {
				contract.setAccptedByResponder(true);
				isUpdated = true;
			}
		} else if (isAccepted.equals("decline")) {
			if (isBettor) {
				contract.setAccptedByBettor(false);
				isUpdated = true;
			}
			if (isResponder) {
				contract.setAccptedByResponder(false);
				isUpdated = true;
			}
		}
		if (isUpdated) {
			System.out.println("updated contract");
			contractsChangeEvent.occur(contracts);
		}
		
		if (contract.isAccptedByBettor() && contract.isAccptedByResponder()) {
			try {
				contract.act();
			} catch (ActionException e) {
				e.printStackTrace();
			}
			this.addNewBlock(contract);
		}
	}

	public void setContractChanged() {
		contractsChangeEvent.occur(contracts);
	}

}
