package models.blocks;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;

public class Block implements Serializable {

	private static final long serialVersionUID = 3407316762547543590L;

	@Override
	public String toString() {
		return "Block [index=" + index + ", previousHash=" + previousHash + ", timestamp=" + timestamp + ", data="
				+ data + ", hash=" + hash + "]";
	}

	private Long index;
	private String previousHash;
	private Timestamp timestamp;
	private Object data;
	private String hash;

	public Block(long index, String previousHash, Timestamp timestamp, Object data, String hash) {
		this(index, previousHash, timestamp, data);
		this.hash = hash;
	}

	public Block(long index, String previousHash, Timestamp timestamp, Object data) {
		this.index = index;
		this.previousHash = previousHash;
		this.timestamp = timestamp;
		this.data = data;
		hash = calculateHash();
		System.out.println("Block [index=" + index + ", previousHash=" + previousHash + ", timestamp=" + timestamp
				+ ", data=" + data + ", hash=" + hash + "]");
	}

	public Block(Block previousBlock, Object data) {
		this(previousBlock.index + 1, previousBlock.hash, new Timestamp(System.currentTimeMillis()), data);
	}

	public static Block getGenesisBlock() {
		// TODO fix hash
		return new Block(0, "0000000000000000000000000000", new Timestamp(System.currentTimeMillis()), "Genesis block",
				"d2468e0cdcb77ef716701e6e33b8599fba94d1b52320d7e7977b1b91d7f1ad96");
	}

	public boolean isValidNextBlock(Block newBlock) {
		// TODO change to exception
		if (this.index + 1 != newBlock.index) {
			// System.out.println("1: " + newBlock.index + "\r\n2: " + index);
			return false;
		} else if (!this.hash.equals(newBlock.previousHash)) {
			// System.out.println("1: " + newBlock.previousHash + "\r\n2: " + hash);
			return false;
		} else if (!(newBlock.isValidHash())) {
			return false;
		}
		return true;
	}

	public boolean isValidHash() {
//		System.out.println("1: " + this.calculateHash() + "\r\n2: " + hash);
//		System.out.println(toString());
		return hash.equals(this.calculateHash());
	}

	private String calculateHash() {
		// TODO LOOK HERE
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-256");
			return getHexStringFromBytes(md.digest(concatData(index, previousHash, timestamp, data)));
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	public static String getHexStringFromBytes(byte[] bytes) {
		final String HEX_FORMAT = "%02x";
		StringBuilder hexStringBuilder = new StringBuilder();
		for (byte b : bytes) {
			hexStringBuilder.append(String.format(HEX_FORMAT, b));
		}
		return hexStringBuilder.toString();
	}

	private static byte[] concatData(Long index, String previousHash, Timestamp timestamp, Object data)
			throws UnsupportedEncodingException {
//		System.out.println(data.toString() + " " + previousHash.toString() + " " + timestamp.toString());
		return new StringBuilder().append(index.toString()).append(previousHash.toString()).append(timestamp.toString())
				.append(data.toString()).toString().getBytes("UTF-8");
	}

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getPreviousHash() {
		return previousHash;
	}

	public void setPreviousHash(String previousHash) {
		this.previousHash = previousHash;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

}
