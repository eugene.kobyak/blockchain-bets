package models.entity;

import java.io.Serializable;

import models.blocks.Blockchain;

public class Transaction implements Serializable, Action {

	private static final long serialVersionUID = -341963479785880392L;
	private String sender;
	private String receiver;
	private int amount;

	public String getSender() {
		return sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public int getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return "Transaction [sender=" + sender + ", receiver=" + receiver + ", amount=" + amount + "]";
	}

	public Transaction(String sender, String receiver, int amount) throws ActionException {
		if (Blockchain.getInstance().getPeople().get(sender).getBankAccount() < amount)
			throw new ActionException("Not enough money");
		this.sender = sender;
		this.receiver = receiver;
		this.amount = amount;
	}

	public Transaction() {
		super();
	}

	@Override
	public void act() throws ActionException {
		Person pSender = Blockchain.getInstance().getPeople().get(sender);
		Person pReceiver = Blockchain.getInstance().getPeople().get(receiver);
		pSender.setBankAccount(pSender.getBankAccount() - amount);
		pReceiver.setBankAccount(pReceiver.getBankAccount() + amount);
		System.out.println(pSender.getName() + ": " + pSender.getBankAccount() + "  " + pReceiver.getName() + ": " + pReceiver.getBankAccount());
	}

}
