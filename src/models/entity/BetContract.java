package models.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class BetContract implements Action, Serializable {

	private static final long serialVersionUID = -3364871123832211019L;
	private String bettor;
	private String responder;
	private int bet;
	private BetCondition condition;
	private String status;
	private Boolean accptedByBettor = null;
	private Boolean accptedByResponder = null;
	private Timestamp time;

	public String getBettor() {
		return bettor;
	}

	public void setBettor(String bettor) {
		this.bettor = bettor;
	}

	public String getResponder() {
		return responder;
	}

	public void setResponder(String responder) {
		this.responder = responder;
	}

	public int getBet() {
		return bet;
	}

	public void setBet(int bet) {
		this.bet = bet;
	}

	public Boolean isAccptedByBettor() {
		return accptedByBettor;
	}

	public void setAccptedByBettor(Boolean eccptedByBettor) {
		this.accptedByBettor = eccptedByBettor;
	}

	public Boolean isAccptedByResponder() {
		return accptedByResponder;
	}

	public void setAccptedByResponder(Boolean eccptedByResponder) {
		this.accptedByResponder = eccptedByResponder;
	}

	public BetCondition getCondition() {
		return condition;
	}

	public void setCondition(BetCondition condition) {
		this.condition = condition;
	}

	public Timestamp getTime() {
		return time;
	}

	public BetContract(String bettor, String responder, int bet, String url) {
		super();
		this.bettor = bettor;
		this.responder = responder;
		this.bet = bet;
		this.time = new Timestamp(System.currentTimeMillis());
		this.condition = new BetCondition(url);
	}

	public void act() throws ActionException {
		System.out.println("hello from contract act");
		Transaction transaction;
		switch (condition.getConditionResult()) {
		case draw:
			status = "Draw";
			break;
		case win:
			status = "First Win";
			transaction = new Transaction(responder, bettor, bet);
			transaction.act();
			break;
		case lose:
			status = "Second Win";
			transaction = new Transaction(bettor, responder, bet);
			transaction.act();
			break;
		case notHappened:
			status = "Not happend";
			ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
			executor.schedule(() -> {
				try {
					this.act();
				} catch (ActionException e) {
					e.printStackTrace();
				}
			}, 1, TimeUnit.HOURS);
			break;
		}
		System.out.println("end of contract act");
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + bet;
		result = prime * result + ((bettor == null) ? 0 : bettor.hashCode());
		result = prime * result + ((condition == null) ? 0 : condition.hashCode());
		result = prime * result + ((responder == null) ? 0 : responder.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BetContract other = (BetContract) obj;
		if (bet != other.bet)
			return false;
		if (bettor == null) {
			if (other.bettor != null)
				return false;
		} else if (!bettor.equals(other.bettor))
			return false;
		if (condition == null) {
			if (other.condition != null)
				return false;
		} else if (!condition.equals(other.condition))
			return false;
		if (responder == null) {
			if (other.responder != null)
				return false;
		} else if (!responder.equals(other.responder))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

	public boolean updateSignatures(BetContract contract) {
		boolean isContractChanged = false;
		if (this.isAccptedByBettor() == null && contract.isAccptedByBettor() != null) {
			this.setAccptedByBettor(contract.isAccptedByBettor());
			isContractChanged = true;
		}
		if (this.isAccptedByResponder() == null && contract.isAccptedByResponder() != null) {
			this.setAccptedByResponder(contract.isAccptedByResponder());
			isContractChanged = true;
		}
		return isContractChanged;
	}

	@Override
	public String toString() {
		return "BetContract [bettor=" + bettor + ", responder=" + responder + ", bet=" + bet + ", condition="
				+ condition + ", status=" + status + "]";
	}

}
