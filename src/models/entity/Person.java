package models.entity;

import java.io.Serializable;

public class Person implements Serializable {

	private static final long serialVersionUID = 3628397438976698532L;
	private int bankAccount;
	private String name;
	private String passwordHash;

	public Person(int bankAccount, String name) {
		this.bankAccount = bankAccount;
		this.name = name;
	}

	public Person(int bankAccount, String name, String passwordHash) {
		this.bankAccount = bankAccount;
		this.name = name;
		this.passwordHash = passwordHash;
	}

	
	public int getBankAccount() {
		return bankAccount;
	}

	public String getName() {
		return name;
	}

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public void setBankAccount(int bankAccount) {
		this.bankAccount = bankAccount;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + "]";
	}
}
