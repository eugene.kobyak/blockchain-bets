package models.entity;

public interface Action {

	void act() throws ActionException;
	
}
