package models.entity;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;

public class BetCondition implements Serializable {

	private static final long serialVersionUID = 1832504644588469278L;

	public static enum ConditionState {
		win, lose, draw, notHappened
	}

	private String url;

	public String getUrl() {
		return url;
	}

	public BetCondition(String url) {
		this.url = url;
	}

	public ConditionState getConditionResult() {
		String data = getValuableData(url);
		if (checkDelay(data)) {
			return ConditionState.draw;
		} else if (checkTime(data)) {
			return ConditionState.notHappened;
		} else {
			String scores[] = getScore(data);
			if (scores.length >= 2) {
				int firstTeamScore = Integer.parseInt(scores[0]);
				int secondTeamScore = Integer.parseInt(scores[1]);
				return firstTeamScore == secondTeamScore ? ConditionState.draw
						: firstTeamScore > secondTeamScore ? ConditionState.win
								: firstTeamScore < secondTeamScore ? ConditionState.lose : ConditionState.notHappened;
			} else {
				return ConditionState.notHappened;
			}
		}
	}

	private static boolean checkDelay(String data) {
		final String DELAY_PATTERN = "[��������������������������������]{3,}";
		return Pattern.compile(DELAY_PATTERN).matcher(data).find();

	}

	private static boolean checkTime(String data) {
		final String TIME_PATTERN = "(\\d\\d:\\d\\d)";
		return Pattern.compile(TIME_PATTERN).matcher(data).find();
	}

	private static String[] getScore(String data) {
		final String SCORE_PATTERN = "(\\d\\s-\\s\\d)";
		Matcher matcher = Pattern.compile(SCORE_PATTERN).matcher(data);
		return matcher.find() ? matcher.group(1).split(" - ") : new String[2];
	}

	private static String getValuableData(String url) {
		String string = "";
		try {
			string = Jsoup.parse(new URL(url).openStream(), "UTF-8", url).select("h3.thick").eachText().get(1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return string;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BetCondition other = (BetCondition) obj;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BetCondition [url=" + url + "]";
	}

}
