"use strict";
function printUserContracts(contracts) {

	var content = document.getElementById("contracts");
	var doc = '';
	contracts
			.forEach(function(value, index, array) {
				var acceptance = "";
				var submit = "";
                var bet = "<label>bet:" + value.bet +"</label>";
				var bettor = "<label> bettor:" + value.bettor + "</label>  "
				var responder = "<label>responder:" + value.responder
						+ "</label>";
				var info = '<a href="' + value.url + '"><u>INFORMATION</u> </a>';
				if (((value.currentUser == value.bettor) && (value.bettorSign == null))
						|| ((value.currentUser == value.responder) && (value.responderSign == null))) {
					acceptance = '<input name="isAccepted' + value.index
							+ '" type="radio" value="accept"> Accept</input>'
							+ '<input name="isAccepted' + value.index
							+ '" type="radio" value="decline" checked> Decline</input><br>';
					submit = '<button type="button" class="btn btn-default" onclick="submitContract('
							+ value.index + ')">Submit</button>'
				}
				doc += '<div class="stick" id="f' + value.index + '">' + bettor + responder
						+ bet + "<br>" + acceptance + info + submit + '</div>';

			});
	content.innerHTML = doc;
}

function submitContract(index) {
	var data = $(
			'div#f' + index + ' input[name="isAccepted' + index + '"]:checked')
			.val();
	var Contract = {
		index : index,
		answer : data
	};
	if (socket.readyState == 1) {
		socket.send(JSON.stringify(Contract));
	} else {
		alert("connection abort");
	}
	// window.open("http://localhost:8080/BlockchainProject/index", "_self");
}

function printBlockchain(chain) {
	var content = document.getElementById("chain");
	content.innerHTML = '';
	for (var i = 0; i < chain.length; i++) {
		content.innerHTML += chain[i].index + " " + chain[i].hash + " "
				+ chain[i].data + '<br>';
	}
}

function onMessage(event) {
	// "use strict";
	try {
		var message = JSON.parse(event.data);
	} catch (err) {
		console.log(err.message);
	}

	if (typeof message.chain !== 'undefined') {
		printBlockchain(message.chain);
		printAccountStatus(message.person);
	}
	if (typeof message.contracts !== 'undefined') {
		printUserContracts(message.contracts)
	}
	return false;
}

function onClose(event) {
	console.log(event);
	socket = new WebSocket("ws://localhost:8080/BlockchainProject/actions");
	socket.onmessage = onMessage;
	socket.onclose = onClose;
}

function printAccountStatus(person) {
	var content = document.getElementById("accountStatus");
	content.innerHTML = '';
	content.innerHTML += '<div><label>Account</label> <br><label>Name: ' + person.currentUser + '</label>'
			+ '<br><label>Amount: ' + person.amount + '</label><br></div>';
}

var socket;
$(document).ready(function() {
	socket = new WebSocket("ws://localhost:8080/BlockchainProject/actions");
	socket.onmessage = onMessage;
	socket.onclose = onClose;
});
