<%@page import="models.entity.Person"%>
<%-- <%@page import="models.entity.BetContract"%>
<%@page import="models.entity.Transaction"%>
<%@page import="models.blocks.Blockchain"%>
<%@page import="models.blocks.Block"%>
<%@page import="java.util.List"%>
 --%>

<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%> --%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="JSP/websocket.js"></script>
<link rel="stylesheet" type="text/css" href="JSP/stylesheet.css">
<title>Blockchain bets</title>
</head>

<body class="body">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6">
				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#panel1">Transaction</a></li>
					<li><a data-toggle="tab" href="#panel2">Bet Contract</a></li>
				</ul>
				<div class="tab-content">
					<div id="panel1" class="tab-pane fade in active">
						<form method="POST" action="/BlockchainProject/TransactionServlet">
							<div class="form-group" >
								<label>Sender</label> <input type="text" class="form-control"
									name="sender" placeholder="Username">
							</div>
							<div class="form-group" >
								<label>Receiver</label> <input type="text" class="form-control"
									name="receiver" placeholder="Username">
							</div>
							<div class="form-group" >
								<label>Amount</label> <input type="number" class="form-control"
									id="exampleInputEmail1" name="amount">
							</div>
							<button type="submit" class="btn btn-default">Send</button>
						</form>
					</div>
					<div id="panel2" class="tab-pane fade">
						<form method="POST" action="/BlockchainProject/ContractServlet">
							<div class="form-group" >
								<label>Bettor</label> <input type="text" class="form-control"
									placeholder="Bettor" name="bettor">
							</div>
							<div class="form-group" >
								<label>Bettor</label> <input type="text" class="form-control"
									placeholder="Bettor" name="respondent">
							</div>
							<div class="form-group" >
								<label>Amount</label> <input type="number" class="form-control"
									name="bet">
							</div>
							<div class="form-group" >
								<label>Url of game</label> <input type="url"
									class="form-control" placeholder="Url" name="url">
							</div>
							<button type="submit" class="btn btn-default">Bet</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div style="margin: 30px 30px;" id="accountStatus">
				</div>
				<div class="stickers pre-scrollable" id="contracts">
					
				</div>
			</div>
		</div>
	</div>
	<div class="navbar-fixed-bottom container" id="asd" style="color: red">
		<div class="text pre-scrollable" style="height: 150px" id="chain">


		</div>
	</div>
</body>
</html>